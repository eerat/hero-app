/**
 * Created by eralp on 22/06/2017.
 */
/*

 function delay(milliseconds: number, count: number): Promise<number> {
 return new Promise<number>(resolve => {
 setTimeout(() => {
 resolve(count);
 }, milliseconds);
 });
 }

 // async function allways return a Promise
 async function dramaticWelcome(): Promise<void> {
 console.log("Hello");

 for (let i = 0; i < 5; i++) {
 // await is converting Promise<number> into number
 const count:number = await delay(500, i);
 console.log(count);
 }

 console.log("World!");
 }

 dramaticWelcome();



------------------------
function resolveAfter2Seconds(x) {
  return new Promise(resolve => {
    setTimeout(() => {
    resolve(x);
  }, 2000);
});
}

async function add1(x) {

  var a = resolveAfter2Seconds(20);
  var b = resolveAfter2Seconds(30);
  return x + await a + await b;
}


add1(10).then(v => {
  console.log(v);
});

async function add2(x) {
  var a = await resolveAfter2Seconds(20);
  var b = await resolveAfter2Seconds(30);
  return x + a + b;
}

add2(10).then(v => {
  console.log(v);
});

function getProcessedData(url) {
  return downloadData(url) // returns a promise
      .catch(e => {
      return downloadFallbackData(url); // returns a promise
})
.then(v => {
    return processDataInWorker(v); // returns a promise
});
}


async function getProcessedData(url) {
  let v;
  try {
    v = await downloadData(url);
  } catch(e) {
    v = await downloadFallbackData(url);
  }
  return processDataInWorker(v);
}

*/
