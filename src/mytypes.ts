/**
 * Created by eralp on 21/06/2017.
 */

export namespace mytypes {
  export class Hero {
    constructor(public id: number,
                public name: string) {

    }
  }
  export class Article {
    constructor(public id: number,
                public created: number,
                public title: string,
                public content: string) {

    }
  }


}
