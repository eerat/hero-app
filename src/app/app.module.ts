import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms'
import {RouterModule, Routes} from '@angular/router'
import {HttpModule} from "@angular/http";

import {HeroDetailComponent} from './hero-detail/hero-detail.component';
import {MyconverterPipe} from './myconverter.pipe';
import {LoadertempComponent} from './loadertemp/loadertemp.component';
import {HeroesComponent} from './heroes.component';
import {DashboardComponent} from './dashboard/dashboard.component';

import {ModalModule, DatepickerModule} from 'ngx-bootstrap';

import {AlertModule} from 'ngx-bootstrap/alert';

import {HeroService} from "./hero-service";

import {AppComponent} from "./app/app.component";
import {MycalendarComponent} from './mycalendar/mycalendar.component';
import {TempComponent} from './temp/temp.component';
import {AnotherService} from "./another.service";
import {Temp2Component} from './temp2/temp2.component';
import {MynewComponent} from './mynew/mynew.component';
import {DetailComponent} from './detail/detail.component';
import {ItemshowComponent} from './itemshow/itemshow.component';

const routes: Routes = [
  //{path: '', redirectTo: '/dashboard', pathMatch: 'full'},
  {path: '', redirectTo: '/', pathMatch: 'full'},
  {path: 'dashboard', component: DashboardComponent},
  {path: 'detail/:id/:name', component: HeroDetailComponent},
  {path: 'heroes', component: HeroesComponent},
  {path: 'articles', component: ArticlelistComponent}
];

//import {InMemoryWebApiModule} from 'angular-in-memory-web-api';
import {InMemoryDataService}  from './in-memory-data.service';
import {HeroSearchComponent} from './hero-search/hero-search.component';
import {ArticlelistComponent} from './articlelist/articlelist.component';


@NgModule({
  declarations: [
    AppComponent,
    HeroDetailComponent,
    MyconverterPipe,
    LoadertempComponent,
    HeroesComponent,
    DashboardComponent,
    MycalendarComponent,
    TempComponent,
    Temp2Component,
    MynewComponent,
    DetailComponent,
    ItemshowComponent,
    HeroSearchComponent,
    ArticlelistComponent
  ],
  imports: [
    BrowserModule, FormsModule, HttpModule,
    RouterModule.forRoot(routes),
    AlertModule.forRoot(),
    ModalModule.forRoot(),
    DatepickerModule.forRoot()
    //,InMemoryWebApiModule.forRoot(InMemoryDataService),
  ],
  providers: [
    HeroService,
    {provide: 'another', useClass: AnotherService},
    {provide: 'tempValue', useValue: "http://www.google.com.tr"}
  ],
  bootstrap: [ArticlelistComponent]
})
export class AppModule {
}
