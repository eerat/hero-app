import {Component} from '@angular/core';

@Component({
  selector: 'my-app',
  template: `
    <h1>{{title}}</h1>

    <nav>
      <a routerLink="/dashboard" routerLinkActive="active">Dashboard</a>
      <a routerLink="/heroes" routerLinkActive="active">Heroes</a>
      <a routerLink="/articles" routerLinkActive="active">Articles</a>

    </nav>

    <router-outlet></router-outlet>


    <!--    

<button type="button" class="btn btn-primary" (click)="staticModal.show()">Static modal</button>

<div class="modal fade" bsModal #staticModal="bs-modal" [config]="{backdrop: 'static'}"
     tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title pull-left">Static modal</h4>
        <button type="button" class="close pull-right" aria-label="Close" (click)="staticModal.hide()">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        This is static modal, backdrop click will not close it.
        Click <b>&times;</b> to close modal.
      </div>
    </div>
  </div>
</div>

this is my calendar!!

<app-mycalendar></app-mycalendar>
-->


    <!--<my-heroes></my-heroes> -->
  `,
  styles: [
      `
      h1 {
        font-size: 1.2em;
        color: #999;
        margin-bottom: 0;
      }

      h2 {
        font-size: 2em;
        margin-top: 0;
        padding-top: 0;
      }

      nav a {
        padding: 5px 10px;
        text-decoration: none;
        margin-top: 10px;
        display: inline-block;
        background-color: #eee;
        border-radius: 4px;
      }

      nav a:visited, a:link {
        color: #607D8B;
      }

      nav a:hover {
        color: #039be5;
        background-color: #CFD8DC;
      }

      nav a.active {
        color: #039be5;
      }
    `
  ]
})
export class AppComponent {
  title = 'Tour of Items';
}
