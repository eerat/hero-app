import {Component, EventEmitter, Inject, Input, OnInit, Output} from '@angular/core';
import {HeroService} from "../hero-service";

@Component({
  selector: 'app-temp',
  template: `
    <p>
      temp Works!
      <input type="text" [(ngModel)]="info" placeholder="info...">
      <button (click)="ClickMe()"></button>
    </p>
  `,
  styles: [],
  providers: [HeroService]
})
export class TempComponent implements OnInit {

  @Input() info;
  @Output() backUpdate = new EventEmitter();

  constructor(@Inject('another') private another,
              @Inject('tempValue') private valuex) {
    //another.hi()
  }

  ngOnInit() {
    /*
    let hs: HeroService = new HeroService();
    hs.getHeroesV2().forEach((item) => {
      console.log(item.name);
    })
    */
  }

  ClickMe() {
    this.backUpdate.emit({"text": this.info});
  }
}
