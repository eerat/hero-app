import {Component} from '@angular/core';
import {Router} from "@angular/router";
import {mytypes} from "../mytypes";
import Hero = mytypes.Hero;
import {HeroService} from "./hero-service";

@Component({
  selector: 'my-heroes',
  template: `
    <h2>My Heroes</h2>
    <div>
      <label>Hero name:</label> <input #heroName/>
      <button (click)="add(heroName.value); heroName.value=''">
        Add
      </button>
    </div>
    <ul class="heroes">
      <li *ngFor="let hero of heroes" (click)="onSelect(hero)"
          [class.selected]="hero === selectedHero">
        <span class="badge">{{hero.id}}</span>
        <span>{{hero.name}}</span>
        <button class="delete"
                (click)="delete(hero); $event.stopPropagation()">x
        </button>
      </li>
    </ul>
    <div *ngIf="selectedHero">
      <h2>
        {{selectedHero.name | uppercase}} is my hero
      </h2>
      <button (click)="gotoDetail()">View Details</button>
    </div>


    <!--
    
    <h2>My Heroes List</h2>
    <div>
      <ul>
        <li *ngFor="let _hero of heroes"
            [class.selected]="_hero === selectedHero"
            (click)="onSelect(_hero)">
          <span class="badge">{{_hero.id}}</span>
          {{_hero.name}}

          <button class="delete"
                  (click)="delete(_hero); $event.stopPropagation()">x</button>
          
        </li>
      </ul>
    </div>


    <div *ngIf="selectedHero">
      <h2>
        {{selectedHero.name | myconverter}} is my hero
      </h2>
      <button (click)="gotoDetail()">View Details</button>
    </div>

    <div>
      <label>Hero name:</label> <input #heroName />
      <button (click)="add(heroName.value); heroName.value=''">
        Add
      </button>
    </div>
    -->

    <!--
    <div *ngIf="selectedHero">
      <span>{{selectedHero.name | myconverter }}</span>
    </div>
    -->
  `,
  styles: [
      `
      label {
        display: inline-block;
        width: 3em;
        margin: .5em 0;
        color: #607D8B;
        font-weight: bold;
      }

      input {
        height: 2em;
        font-size: 1em;
        padding-left: .4em;
      }

      button {
        margin-top: 20px;
        font-family: Arial;
        background-color: #eee;
        border: none;
        padding: 5px 10px;
        border-radius: 4px;
        cursor: pointer;
        cursor: hand;
      }

      button:hover {
        background-color: #cfd8dc;
      }

      button:disabled {
        background-color: #eee;
        color: #ccc;
        cursor: auto;
      }

      button.delete {
        float: right;
        margin-top: 2px;
        margin-right: .8em;
        background-color: gray !important;
        color: white;
      }
    `
  ]
})
export class HeroesComponent {
  title = 'My Hero Application';
  heroes: Hero[];
  selectedHero: Hero;

  constructor(private heroService: HeroService
    , private router: Router) {
    /*
     this.hero =
     new Hero(1, "Ahmet")
     */
    this.getHeroes();
  }

  delete(hero: Hero): void {
    this.heroService
      .delete(hero.id)
      .then(() => {
        this.heroes = this.heroes.filter(h => h !== hero);
        if (this.selectedHero === hero) {
          this.selectedHero = null;
        }
      });
  }

  onSelect(myHero) {
    this.selectedHero = myHero;
  }

  elma(selectedItem: Hero): boolean {
    //if (selectedItem.name =='er') {
    return true;
    //} else return false;
  }

  getHeroes(): void {
    this.heroService.getHeroes().then(items =>
      this.heroes = items);
  }

  gotoDetail(): void {
    this.router.navigate(['/detail', this.selectedHero.id, this.selectedHero.name]);
  }

  add(name: string): void {
    name = name.trim();
    if (!name) {
      return;
    }
    this.heroService.create(name)
      .then(hero => {
        this.heroes.push(hero);
        this.selectedHero = null;
      });
  }

}
/*
 const HEROES: Hero[] = [
 {id: 11, name: 'Mr. Nice'},
 {id: 12, name: 'Narco'},
 {id: 13, name: 'Bombasto'},
 {id: 14, name: 'Celeritas'},
 {id: 15, name: 'Magneta'},
 {id: 16, name: 'RubberMan'},
 {id: 17, name: 'Dynama'},
 {id: 18, name: 'Dr IQ'},
 {id: 19, name: 'Magma'},
 {id: 20, name: 'Tornado'}
 ];
 */
