import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-temp2',
  template: `
    <p>
      {{name}}
      <app-temp (backUpdate)="UpdateMe(name)" [info]="name"  ></app-temp>
    </p>
  `,
  styles: []
})
export class Temp2Component implements OnInit {

  name;
  constructor() { }

  ngOnInit() {
  }

  UpdateMe(information) {
    console.log(information)
  }
}
