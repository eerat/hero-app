import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-detail',
  template: `
    <p>
      <input type="text" [(ngModel)]="name" placeholder="name info ">
      <button (click)="SendToBack(name)">
        Send to Back!
      </button>
      <!-- (keyup)="sender.emit({nameInfo:name})" -->
    </p>
  `,
  styles: []
})
export class DetailComponent implements OnInit {

  @Input() enterPoint;
  @Output() sender = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
  }

  SendToBack(event) {
    this.sender.emit({text:event})
  }

}
