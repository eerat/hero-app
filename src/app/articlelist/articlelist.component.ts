import {Component, ElementRef, Input, ViewChild} from '@angular/core';
import {mytypes} from "../../mytypes";
import Article = mytypes.Article;
import {HeroService} from "../hero-service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-articlelist',
  template: `

    <div class="container-fluid" *ngIf="this.articles?.length>0">
      <div class="row">
        <hr>
      </div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
          <h3><span style="display: inline-block; width: 100%;"
                    class="label label-primary">Article List [ Total : # {{this.articles?.length}} ] </span></h3>
        </div>
      </div>
    </div>

    <li *ngFor="let article of articles" (click)="onSelect(article)"
        [class.selected]="article === selectedArticle">
      <span style="display:inline-block;width: 40px; color:white; background-color: red"
            class="badge">{{article.id}}</span>
      <span style="display: inline-block; width: 200px;">{{article.title}}</span>
      <button style="margin-left: 10px;" class="delete"
              (click)="delete(article); $event.stopPropagation()">
        <span class="glyphicon glyphicon-remove text-danger" aria-hidden="true"></span>

      </button>
      <button style="margin-left: 10px;" class="edit"
              (click)="edit(article); $event.stopPropagation()">
        <span class="glyphicon glyphicon-pencil text-warning" aria-hidden="true"></span>

      </button>

    </li>


    <div class="container-fluid" *ngIf="this.articles?.length>0">
      <div class="row">
        <hr>
      </div>
    </div>


    <div *ngIf="this.IsDeleting">
      Deleting..!
    </div>
    <div *ngIf="this.IsSaving">
      Saving..!
    </div>

    <div *ngIf="this.articles?.length>0">
      <br/>
      <br/>
      <br/>
    </div>

    <div class="container-fluid" *ngIf="this.articles?.length>0">
      <div class="row">
        <div style="margin-top: 10px;" class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
          <button *ngIf="IsShowPanel" #btnShowAddPanel class="form-control" id="btnShowAddPanel"
                  (click)="ShowAddPanel();"
                  class="btn btn-success">

            <span class="glyphicon glyphicon-plus text-success" style="color:white;font-family: Menlo;"
                  aria-hidden="true"> New Article</span>


          </button>
        </div>

      </div>
    </div>


    <div [hidden]="!this.IsNewRecord">

      <form #articleForm="ngForm" (ngSubmit)="onSubmit()">

        <div *ngIf="this.IsNewRecord || this.IsUpdating">
          <hr>
          <div *ngIf="this.IsNewRecord && !this.IsUpdating">
            <strong>New Article</strong>
          </div>
          <div *ngIf="this.IsUpdating && !this.IsNewRecord">
            <strong>Edit Article</strong>
          </div>
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                <div class="input-group">
                  <table>
                    <tr>
                      <td>
                        <label for="txtTitle" style="width: 200px;">
                          <span class="label label-info" style="width: 100px;">Title</span>
                        </label>
                      </td>
                      <td>
                        <input type="text" id="txtTitle" #txtTitle="ngModel"
                               name="txtTitle" style="width: 300px;" class="form-control"
                               placeholder="Title"
                               required minlength="1" maxlength="50"
                               [(ngModel)]="_title">
                      </td>
                    </tr>
                    <tr>
                      <td colspan="2">
                        <div style="margin-top: 10px;" *ngIf="txtTitle.errors && (txtTitle.dirty || txtTitle.touched)"
                             class="alert alert-danger">
                          <div [hidden]="!txtTitle.errors.required">
                            Title is required
                          </div>
                          <div [hidden]="!txtTitle.errors.minlength">
                            Title must be at least 1 characters long.
                          </div>
                          <div [hidden]="!txtTitle.errors.maxlength">
                            Title cannot be more than 50 characters long.
                          </div>
                        </div>
                      </td>
                    </tr>
                  </table>
                </div>
              </div>
              <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                <div class="input-group">
                  <table>
                    <tr>
                      <td>
                        <label for="txtContent" style="width: 200px;">
                          <span class="label label-info" style="width: 100px;">Content</span>
                        </label>
                      </td>
                      <td>
                        <input type="text" id="txtContent" #txtContent="ngModel"
                               name="txtContent" style="margin-top: 10px; width: 300px;" class="form-control"
                               placeholder="Content"
                               required minlength="1" maxlength="50"
                               [(ngModel)]="_content">
                      </td>
                    </tr>
                    <tr>
                      <td colspan="2">
                        <div style="margin-top: 10px;"
                             *ngIf="txtContent.errors && (txtContent.dirty || txtContent.touched)"
                             class="alert alert-danger">
                          <div [hidden]="!txtContent.errors.required">
                            Content is required
                          </div>
                          <div [hidden]="!txtContent.errors.minlength">
                            Content must be at least 1 characters long.
                          </div>
                          <div [hidden]="!txtContent.errors.maxlength">
                            Content cannot be more than 50 characters long.
                          </div>
                        </div>
                      </td>
                    </tr>
                  </table>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-xs-6 col-sm-6 col-lg-6">
                <div class="row">
                  <div style="margin-top: 10px;" class="col-md-10 col-xs-10 col-sm-10 col-lg-10">
                    <button type="submit" (click)="Save(txtTitle.value,txtContent.value)"
                            [disabled]="!articleForm.form.valid" #btnAddArticle id="btnAddArticle"
                            class="btn btn-primary">
                      {{!this.IsUpdating ? "Add New Article" : "Edit Article"}}
                    </button>
                  </div>

                  <div style="margin-top: 10px;" class="col-md-2 col-xs-2 col-sm-2 col-lg-2">
                    <button #btnCancel id="btnCancel" (click)="Cancel($event);" class="btn btn-warning">Cancel</button>
                  </div>

                </div>
              </div>
              <div class="col-md-6 col-xs-6 col-sm-6 col-lg-6">
              </div>
            </div>
          </div>
          <hr>
        </div>

      </form>

    </div>
  `,
  styles: [`
    hr {
      display: block;
      height: 1px;
      border: 0;
      border-top: 1px solid #ccc;
      margin: 1em 0;
      padding: 0;
      border-color: red;
    }
  `]
})

export class ArticlelistComponent {

  @Input() _title;
  @Input() _content;

  @ViewChild('btnShowAddPanel') btnShowAddPanel: ElementRef;

  submitted: boolean = false;
  articles: Article[];
  selectedArticle;
  public IsDeleting: boolean = false;
  public IsSaving: boolean = false;
  public IsUpdating: boolean = false;
  public IsNewRecord: boolean = false;
  public IsShowPanel: boolean = true;

  ShowAddPanel() {
    this.IsNewRecord = true;
    this.IsUpdating = false;
    this.IsShowPanel = false;
  }

  Save(title, content) {
    this.IsSaving = true;
    this.heroService
      .CreateArticle(title, content)
      .then((result) => {
        this.articles.push(new Article(parseInt(result, 0), -1, title, content));

        let inputFields: any = document.querySelectorAll('input');
        if (inputFields) {
          inputFields.forEach((f) => {
            let element: HTMLInputElement = (<HTMLInputElement> f);
            element.value = "";
          });
        }
        this.IsSaving = false;
        this.ngAfterViewInit();
      });
  }

  ngAfterViewInit() {
    let inputFields: any = document.querySelectorAll('input');
    inputFields && inputFields.length > 0 && inputFields[0].focus();
  }

  onSubmit() {
    this.IsSaving = true;
    this.submitted = true;
  }

  constructor(public heroService: HeroService
    , private router: Router) {
    this.getArticles();
  }

  Cancel(event) {
    this.IsUpdating = false;
    this.IsNewRecord = false;
    this.IsShowPanel = true;
  }

  delete(article: Article): void {
    this.IsDeleting = true;
    this.heroService
      .DeleteArticle(article.id)
      .then((result) => {
        this.articles = this.articles.filter(h => h !== article);
        if (this.selectedArticle === article) {
          this.selectedArticle = null;
        }
        this.IsDeleting = false;
      });
  }

  onSelect(article) {
    this.selectedArticle = article;
  }

  edit(article: Article) {
    alert(article.title + " -> Kiss your hands ! ( completion of this PoC is totally up to you!)")

  }

  getArticles(): void {
    this.heroService.GetArticles().then(items =>
      this.articles = items);
  }

}
