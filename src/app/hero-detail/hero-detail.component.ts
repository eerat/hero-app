import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Params} from "@angular/router";
import {Location} from '@angular/common';
import {mytypes} from "../../mytypes";
import Hero = mytypes.Hero;
import {HeroService} from "../hero-service";
import "rxjs/add/operator/switchMap";

@Component({
  selector: 'hero-detail',
  template: `
    <div *ngIf="hero">
      <h2> {{hero.name | myconverter }} details!</h2>
      <div><label>id : </label>{{hero.id}}</div>
      <div>
        <label>name : </label>
        <input type="text" id="name2" class="form-control"
               required minlength="4" maxlength="24"
               name="name2" [(ngModel)]="hero.name"
               #name2="ngModel">
      </div>

      <div *ngIf="name2.errors && (name2.dirty || name2.touched)"
           class="alert alert-danger">
        <div [hidden]="!name2.errors.required">
          Name is required
        </div>
        <div [hidden]="!name2.errors.minlength">
          Name must be at least 4 characters long.
        </div>
        <div [hidden]="!name2.errors.maxlength">
          Name cannot be more than 24 characters long.
        </div>
      </div>

    </div>

    <button (click)="Save()">Save</button>  
    
    <button (click)="goBack()">Back</button>

  `,
  styleUrls: ['./hero-detail.component.css']
})
export class HeroDetailComponent implements OnInit {

  constructor(private heroService: HeroService,
              private route: ActivatedRoute,
              private location: Location) {
  }

  @Input() hero: Hero;

  ngOnInit() {

    console.log(this.route.params['name']);

    this.route.params
      .switchMap((params: Params) => this.heroService.getHero(+params['id']))
      .subscribe(hero => this.hero = hero);
  }

  goBack(): void {
    this.location.back();
  }

  Save():void {
    this.heroService.update(this.hero).then(()=>this.goBack());
  }

}
