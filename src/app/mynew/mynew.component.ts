import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-mynew',
  template: `
    Hello {{title}}
    <app-detail [enterPoint]="info" #appdetail1 (sender)="DoSomething($event)">

    </app-detail>


    <app-detail [enterPoint]="info2" #appdetail2 (sender)="DoSomething2($event)">

    </app-detail>

    <button (click)="Elma($event,'A','B')">Click ME</button>


    <alert type="success">hello</alert>
  `,
  styles: []
})
export class MynewComponent implements OnInit {

  info;

  info2;


  title = "Input / output test";

  constructor() {
  }

  ngOnInit() {
  }

  DoSomething(event) {
     this.info2 =  event.text
  }
  DoSomething2(event) {
  }

  Elma(event, a, b) {
    console.log(event + " , " + a + "," + b)
  }
}
