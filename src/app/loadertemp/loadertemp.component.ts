import {Component, OnInit} from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map'

@Component({
  selector: 'apploader',
  template: `
    <div>
      <div *ngIf="isButtonClicked">
        <div *ngIf="person |async as persona; else loading">
          <span>{{persona.name}} </span> <br/>
          <span>{{persona.lastname}} </span> <br/>
        </div>
        <ng-template #loading>
          Loading..
        </ng-template>
      </div>
    </div>

    /*
    <button (click)="LoadMyData(txtSecond)"  >
      Fill Data
    </button>
    */
    <button (click)="LoadMyData($event,txtSecond)"  >
      Fill Data Full
    </button>

    <label for="txtSecond2">Send Info</label>
    <input type="text" #txtSecond id="txtSecond2">
  `
})
export class LoadertempComponent implements OnInit {
  person;
  isButtonClicked;

  constructor(private http: Http) {
    this.isButtonClicked = false;
  }

  ngOnInit() {
  }

  LoadMyData(event,element) {
    console.log(event,element.value)
    this.isButtonClicked = true;
    this.person =
      this.http.get("/assets/data.json").map(result => result.json())

  }

}
