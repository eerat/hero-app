import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadertempComponent } from './loadertemp.component';

describe('LoadertempComponent', () => {
  let component: LoadertempComponent;
  let fixture: ComponentFixture<LoadertempComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoadertempComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadertempComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
