import {Injectable} from '@angular/core';
import {mytypes} from "../mytypes";
import Hero = mytypes.Hero;
import {HEROES} from './../mock-data'
import {Http, Headers} from "@angular/http";
import "rxjs/add/operator/toPromise";
import "rxjs/add/operator/delay";
import Article = mytypes.Article;

@Injectable()
export class HeroService {

  private allArticlesUrl = "http://localhost:8010/articles_all"
  private allArticlesUrlCUD = "http://localhost:8010/articles"
  private heroesUrl = 'api/heroes';  // URL to web api
  private headers = new Headers({'Content-Type': 'application/json'});

  constructor(private http: Http) {
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

  UpdateArticle(article: Article): Promise<Article> {
    const url = `${this.allArticlesUrlCUD}/${article.id}`;
    return this.http
      .put(url, JSON.stringify(article), {headers: this.headers})
      .toPromise()
      .then(() => article)
      .catch(this.handleError);
  }

  DeleteArticle(id: number): Promise<void> {
    const url = `${this.allArticlesUrlCUD}/${id}`;
    return this.http.delete(url, {headers: this.headers})
      .delay(2000) // response waiting effect :)
      .toPromise()
      .then((result) => {
        return result.json().data;
      })
      .catch(this.handleError);
  }

  CreateArticle(title: string, content: string): Promise<string> {
    return this.http
      .post(this.allArticlesUrlCUD,
        JSON.stringify({title: title, content: content}), {headers: this.headers})
      .delay(2000) // response waiting effect!
      .toPromise()
      .then(res => {
        return res.json().data;
      }).catch(this.handleError);
  }

  GetArticles(): Promise<Article[]> {
    return this.http.get(this.allArticlesUrl)
      .toPromise()
      .then(response => {
        let myArticleList: Article[] = [];
        response.json().data.forEach((item) => {
          let _article: Article = new Article(
            item.id,
            item.created,
            item.title,
            item.content);
          myArticleList.push(_article);
        });
        //response.json().data as Article[];
        return myArticleList;
      })
      .catch(this.handleError);
  }


  /* ****************************** HERO OPERATIONS ****************************** */

  getHeroes_Old(): Promise<Hero[]> {
    return Promise.resolve(HEROES);
  }

  getHeroes(): Promise<Hero[]> {
    return this.http.get(this.heroesUrl)
      .toPromise()
      .then(response => response.json().data as Hero[])
      .catch(this.handleError);
  }

  getHero(id: number): Promise<Hero> {
    const url = `${this.heroesUrl}/${id}`;
    return this.http.get(url)
      .toPromise()
      .then(response => response.json().data as Hero)
      .catch(this.handleError);
  }

  delete(id: number): Promise<void> {
    const url = `${this.heroesUrl}/${id}`;
    return this.http.delete(url, {headers: this.headers})
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  update(hero: Hero): Promise<Hero> {
    const url = `${this.heroesUrl}/${hero.id}`;
    return this.http
      .put(url, JSON.stringify(hero), {headers: this.headers})
      .toPromise()
      .then(() => hero)
      .catch(this.handleError);
  }

  getHeroesV2(): Hero[] {
    return HEROES;
  }

  getHeroesSlowMode(): Promise<Hero[]> {
    return new Promise(resolve => {
      setTimeout(() =>
        resolve(this.getHeroes()), 2000);
    })
  }

  create(name: string): Promise<Hero> {
    return this.http
      .post(this.heroesUrl, JSON.stringify({name: name}), {headers: this.headers})
      .toPromise()
      .then(res => res.json().data as Hero)
      .catch(this.handleError);
  }

  getHero_Old(id: number): Promise<Hero> {
    return this.getHeroes()
      .then(heroes => heroes.find(hero => hero.id === id));
  }

  /* ****************************** HERO OPERATIONS ****************************** */


}
