import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'myconverter'
})
export class MyconverterPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return value.toUpperCase()
  }

}
