import { HeroappPage } from './app.po';

describe('heroapp App', () => {
  let page: HeroappPage;

  beforeEach(() => {
    page = new HeroappPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
