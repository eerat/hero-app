function do1() {
    console.log("1")
    console.log("2")
}

function* do2(){
   console.log("1")
   console.log("2")
}

function* do3(){
   yield 1
   yield {name:"eralp",surname:"erat"}
   yield 3
}

function* do4() { 
    let value1 = yield;
    console.log("Retrieved Value #1->",value1);
    let value2 = yield;
    console.log("Retrieved Value #2->",value2);
}

function* do2And4(){
   yield 2
   yield 4
}

function *do5() {
    yield 1
    const do2And4Caller = do2And4();
    yield *do2And4Caller
    yield 3
    //yield *do2And4Caller.next().value
    yield 5
}


const doCaller = do5();

for(let x of doCaller) {
   console.log(`Value -> ${x}`);
}


//doCaller.next();
//for(let f = 0; f<10;f++) {
//   doCaller.next(f);
//}

/*
while(true) {
    let retValue = doCaller.next();
    if (retValue.done) {
        console.log("completed!")
        break;  
    }
    console.log(`Value -> ${retValue.value}`); 
}
*/
//console.log(doCaller.next());
//console.log(doCaller.next());
//console.log(doCaller.next());
//console.log(doCaller.next());
//doCaller().next();
//console.log(doCaller().next());
//console.log(doCaller().next());
//console.log(doCaller().next());

